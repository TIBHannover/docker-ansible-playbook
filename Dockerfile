FROM alpine:3.14

ARG ANSIBLE_VERSION

RUN mkdir -p /ansible/playbooks
WORKDIR /ansible/playbooks

RUN apk --update --no-cache add \
        ca-certificates \
        git \
        curl \
        jq \
        openssh-client \
        openssl \
        python3\
        py3-pip \
        py3-cryptography \
        rsync \
        sshpass

RUN apk --update add --virtual build-dependencies \
        python3-dev \
        libffi-dev \
        openssl-dev \
        build-base \
 && pip3 install --upgrade pip cffi \
 && pip3 install ansible==$ANSIBLE_VERSION \
 && apk del build-dependencies \
 && rm -rf /var/cache/apk/* \
 && mkdir -p /etc/ansible \
 && echo 'localhost' > /etc/ansible/hosts

CMD [ "ansible-playbook", "--version" ]
