# Docker Ansible-Playbook

Docker images for running Ansible playbooks

## Usage

### Run playbook
```
docker run -it --rm -v ${pwd}:/ansible/playbooks registry.gitlab.com/tibhannover/docker-ansible-playbook ansible-playbook -i inventory.yml playbook.yml
```

### Use in gitlab-ci
* Set the variables *SSH_PRIVATE_KEY* and *SSH_KNOWN_HOSTS* to access your target hosts in the config-repo in gitlab: **Settings -> CI / CD -> Variables**
* *.gitlab-ci.yml*:
```
.ansible_job:
  image: registry.gitlab.com/tibhannover/docker-ansible-playbook:2.9.22
  stage: deploy
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | base64 -d | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - ansible-playbook -v -i inventory.yml playbook.yml
```
